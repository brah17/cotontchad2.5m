<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ordreservice
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Ordreservice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=255)
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="etat_service", type="string", length=255)
     */
    private $etatService;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_transmission", type="integer")
     */
    private $idTransmission;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_user", type="integer")
     */
    private $idUser;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Ordreservice
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set etatService
     *
     * @param string $etatService
     * @return Ordreservice
     */
    public function setEtatService($etatService)
    {
        $this->etatService = $etatService;

        return $this;
    }

    /**
     * Get etatService
     *
     * @return string 
     */
    public function getEtatService()
    {
        return $this->etatService;
    }

    /**
     * Set idTransmission
     *
     * @param integer $idTransmission
     * @return Ordreservice
     */
    public function setIdTransmission($idTransmission)
    {
        $this->idTransmission = $idTransmission;

        return $this;
    }

    /**
     * Get idTransmission
     *
     * @return integer 
     */
    public function getIdTransmission()
    {
        return $this->idTransmission;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     * @return Ordreservice
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return integer 
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}
