<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Boncommande
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Boncommande
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numcom", type="string", length=255)
     */
    private $numcom;

    /**
     * @var string
     *
     * @ORM\Column(name="id_user", type="string", length=255)
     */
    private $idUser;

    /**
     * @var string
     *
     * @ORM\Column(name="datecom", type="string", length=255)
     */
    private $datecom;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="heure_debut", type="string", length=255)
     */
    private $heureDebut;

    /**
     * @var string
     *
     * @ORM\Column(name="heure_fin", type="string", length=255)
     */
    private $heureFin;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numcom
     *
     * @param string $numcom
     * @return Boncommande
     */
    public function setNumcom($numcom)
    {
        $this->numcom = $numcom;

        return $this;
    }

    /**
     * Get numcom
     *
     * @return string 
     */
    public function getNumcom()
    {
        return $this->numcom;
    }

    /**
     * Set idUser
     *
     * @param string $idUser
     * @return Boncommande
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return string 
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set datecom
     *
     * @param string $datecom
     * @return Boncommande
     */
    public function setDatecom($datecom)
    {
        $this->datecom = $datecom;

        return $this;
    }

    /**
     * Get datecom
     *
     * @return string 
     */
    public function getDatecom()
    {
        return $this->datecom;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Boncommande
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set heureDebut
     *
     * @param string $heureDebut
     * @return Boncommande
     */
    public function setHeureDebut($heureDebut)
    {
        $this->heureDebut = $heureDebut;

        return $this;
    }

    /**
     * Get heureDebut
     *
     * @return string 
     */
    public function getHeureDebut()
    {
        return $this->heureDebut;
    }

    /**
     * Set heureFin
     *
     * @param string $heureFin
     * @return Boncommande
     */
    public function setHeureFin($heureFin)
    {
        $this->heureFin = $heureFin;

        return $this;
    }

    /**
     * Get heureFin
     *
     * @return string 
     */
    public function getHeureFin()
    {
        return $this->heureFin;
    }
}
