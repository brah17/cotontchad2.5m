<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trans
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Trans
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="heure_debut", type="string", length=255)
     */
    private $heureDebut;

    /**
     * @var string
     *
     * @ORM\Column(name="heure_fin", type="string", length=255)
     */
    private $heureFin;

    /**
     * @var string
     *
     * @ORM\Column(name="lieu", type="string", length=255)
     */
    private $lieu;

    /**
     * @var string
     *
     * @ORM\Column(name="etat", type="string", length=255)
     */
    private $etat;

    /**
     * @var string
     *
     * @ORM\Column(name="date", type="string", length=255)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="montantdevis", type="string", length=255)
     */
    private $montantdevis;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_equipement", type="integer")
     */
    private $idEquipement;

    /**
     * @var string
     *
     * @ORM\Column(name="id_user", type="string", length=255)
     */
    private $idUser;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set heureDebut
     *
     * @param string $heureDebut
     * @return Trans
     */
    public function setHeureDebut($heureDebut)
    {
        $this->heureDebut = $heureDebut;

        return $this;
    }

    /**
     * Get heureDebut
     *
     * @return string 
     */
    public function getHeureDebut()
    {
        return $this->heureDebut;
    }

    /**
     * Set heureFin
     *
     * @param string $heureFin
     * @return Trans
     */
    public function setHeureFin($heureFin)
    {
        $this->heureFin = $heureFin;

        return $this;
    }

    /**
     * Get heureFin
     *
     * @return string 
     */
    public function getHeureFin()
    {
        return $this->heureFin;
    }

    /**
     * Set lieu
     *
     * @param string $lieu
     * @return Trans
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return string 
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * Set etat
     *
     * @param string $etat
     * @return Trans
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set date
     *
     * @param string $date
     * @return Trans
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return string 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set montantdevis
     *
     * @param string $montantdevis
     * @return Trans
     */
    public function setMontantdevis($montantdevis)
    {
        $this->montantdevis = $montantdevis;

        return $this;
    }

    /**
     * Get montantdevis
     *
     * @return string 
     */
    public function getMontantdevis()
    {
        return $this->montantdevis;
    }

    /**
     * Set idEquipement
     *
     * @param integer $idEquipement
     * @return Trans
     */
    public function setIdEquipement($idEquipement)
    {
        $this->idEquipement = $idEquipement;

        return $this;
    }

    /**
     * Get idEquipement
     *
     * @return integer 
     */
    public function getIdEquipement()
    {
        return $this->idEquipement;
    }

    /**
     * Set idUser
     *
     * @param string $idUser
     * @return Trans
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return string 
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}
