<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Facture
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Facture
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_transmission", type="integer")
     */
    private $idTransmission;

    /**
     * @var string
     *
     * @ORM\Column(name="date_facture", type="string", length=255)
     */
    private $dateFacture;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idTransmission
     *
     * @param integer $idTransmission
     * @return Facture
     */
    public function setIdTransmission($idTransmission)
    {
        $this->idTransmission = $idTransmission;

        return $this;
    }

    /**
     * Get idTransmission
     *
     * @return integer 
     */
    public function getIdTransmission()
    {
        return $this->idTransmission;
    }

    /**
     * Set dateFacture
     *
     * @param string $dateFacture
     * @return Facture
     */
    public function setDateFacture($dateFacture)
    {
        $this->dateFacture = $dateFacture;

        return $this;
    }

    /**
     * Get dateFacture
     *
     * @return string 
     */
    public function getDateFacture()
    {
        return $this->dateFacture;
    }
}
