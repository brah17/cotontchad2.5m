<?php

namespace TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use TestBundle\Entity\User;



class DetailsuserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            
            ->add('email','text',array('attr' => array('class' => 'form-control')))
            ->add('nom','text',array('attr' => array('class' => 'form-control')))
            ->add('prenom','text',array('attr' => array('class' => 'form-control')))
          
 ->add('role_user', 'choice', array(
    'choices' => array('dir_dex' => 'Direction dex', 'tech_dex' => 'Technique dex','dac' => 'Dac')
,'attr' => array('class' => 'form-control')))

            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TestBundle\Entity\Detailsuser'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'testbundle_detailsuser';
    }
}
