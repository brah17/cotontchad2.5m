<?php

namespace TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TransmissionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('heureDebut','text',array('attr' => array('class' => 'form-control')))
            ->add('heureFin','text',array('attr' => array('class' => 'form-control')))
            ->add('lieu','text',array('attr' => array('class' => 'form-control')))
            ->add('etat','text',array('attr' => array('class' => 'form-control')))
            ->add('date','text',array('attr' => array('class' => 'form-control')))
            ->add('montantdevis','text',array('attr' => array('class' => 'form-control')))
           
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TestBundle\Entity\Transmission'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'testbundle_transmission';
    }
}
