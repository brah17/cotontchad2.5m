<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TechDexController extends Controller
{
    public function homeAction()
    {
        return $this->render('TestBundle:TechDex:home.html.twig', array(
                // ...
            ));    }

    public function consulter_ordreserviceAction(){
    	 $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TestBundle:Ordreservice')->findAll();

        return $this->render('TestBundle:TechDex:consulter_ordreservice.html.twig', array(
            'entities' => $entities,
        ));
    }

}
