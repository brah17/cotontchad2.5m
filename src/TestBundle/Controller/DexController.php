<?php

namespace TestBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use TestBundle\Entity\Dex;
use TestBundle\Form\DexType;

/**
 * Dex controller.
 *
 */
class DexController extends Controller
{

    /**
     * Lists all Dex entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TestBundle:Dex')->findAll();

        return $this->render('TestBundle:Dex:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Dex entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Dex();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('dex_show', array('id' => $entity->getId())));
        }

        return $this->render('TestBundle:Dex:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Dex entity.
     *
     * @param Dex $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Dex $entity)
    {
        $form = $this->createForm(new DexType(), $entity, array(
            'action' => $this->generateUrl('dex_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Dex entity.
     *
     */
    public function newAction()
    {
        $entity = new Dex();
        $form   = $this->createCreateForm($entity);

        return $this->render('TestBundle:Dex:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Dex entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TestBundle:Dex')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Dex entity.');
        }

        $deleteForm = $this->createDeleteForm($id)->add('Delete','submit',array( 'attr' => array('class' => 'btn btn-danger')));

        return $this->render('TestBundle:Dex:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Dex entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TestBundle:Dex')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Dex entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TestBundle:Dex:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Dex entity.
    *
    * @param Dex $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Dex $entity)
    {
        $form = $this->createForm(new DexType(), $entity, array(
            'action' => $this->generateUrl('dex_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('Edit', 'submit', array(
    'attr' => array('class' => 'btn btn-success')));

        return $form;
    }
    /**
     * Edits an existing Dex entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TestBundle:Dex')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Dex entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('dex_edit', array('id' => $id)));
        }

        return $this->render('TestBundle:Dex:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Dex entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TestBundle:Dex')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Dex entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('dex'));
    }

    /**
     * Creates a form to delete a Dex entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('dex_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('Delete','submit',array( 'attr' => array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }
}
