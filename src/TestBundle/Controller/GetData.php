<?php

namespace TestBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use TestBundle\Entity\Dex;
use TestBundle\Form\DexType;
use TestBundle\Entity\User;

/**
 * Dex controller.
 *
 */
class GetData extends Controller
{
   public function users(){
   	  $em = $this->getDoctrine()->getManager();

      $entities = $em->getRepository('TestBundle:User')->findAll();
      return $entities;
   }
}