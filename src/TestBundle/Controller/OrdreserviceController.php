<?php

namespace TestBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use TestBundle\Entity\Ordreservice;
use TestBundle\Form\OrdreserviceType;

/**
 * Ordreservice controller.
 *
 */
class OrdreserviceController extends Controller
{

    /**
     * Lists all Ordreservice entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TestBundle:Ordreservice')->findAll();

        return $this->render('TestBundle:Ordreservice:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Ordreservice entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Ordreservice();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('ordreservice_show', array('id' => $entity->getId())));
        }

        return $this->render('TestBundle:Ordreservice:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Ordreservice entity.
     *
     * @param Ordreservice $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Ordreservice $entity)
    {
        $form = $this->createForm(new OrdreserviceType(), $entity, array(
            'action' => $this->generateUrl('ordreservice_create'),
            'method' => 'POST',
        ));
         $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TestBundle:Transmission')->findAll();
        $entities1 = $em->getRepository('TestBundle:User')->findAll();
         $ar=array();
         $ar1=array();
        foreach ($entities as $entity) {
            $ar[$entity->getId()]=$entity->getId();
        }
         foreach ($entities1 as $entity) {
            $ar1[$entity->getId()]=$entity->getUsername();
        }

      
     $form     ->add('idTransmission', 'choice', array(
    'choices' => $ar
,'attr' => array('class' => 'form-control')));
      $form     ->add('idUser', 'choice', array(
    'choices' => $ar1
,'attr' => array('class' => 'form-control')));


        $form->add('submit', 'submit', array('label' => 'Create','attr' => array('class' => 'btn btn-danger')));

        return $form;
    }

    /**
     * Displays a form to create a new Ordreservice entity.
     *
     */
    public function newAction()
    {
        $entity = new Ordreservice();
        $form   = $this->createCreateForm($entity);

        return $this->render('TestBundle:Ordreservice:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Ordreservice entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TestBundle:Ordreservice')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ordreservice entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TestBundle:Ordreservice:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Ordreservice entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TestBundle:Ordreservice')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ordreservice entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TestBundle:Ordreservice:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Ordreservice entity.
    *
    * @param Ordreservice $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Ordreservice $entity)
    {
        $form = $this->createForm(new OrdreserviceType(), $entity, array(
            'action' => $this->generateUrl('ordreservice_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update','attr' => array('class' => 'btn btn-danger')));

        return $form;
    }
    /**
     * Edits an existing Ordreservice entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TestBundle:Ordreservice')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ordreservice entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('ordreservice_edit', array('id' => $id)));
        }

        return $this->render('TestBundle:Ordreservice:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Ordreservice entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TestBundle:Ordreservice')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Ordreservice entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('ordreservice'));
    }

    /**
     * Creates a form to delete a Ordreservice entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ordreservice_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete','attr' => array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }
}
