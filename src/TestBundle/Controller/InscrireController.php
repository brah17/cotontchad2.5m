<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TestBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use TestBundle\Entity\Detailsuser; 
use Symfony\Component\HttpFoundation\Session\Session;

class InscrireController extends Controller
{
    public function step1Action()
    {
        return $this->render('TestBundle:Inscrire:step1.html.twig', array(
                // ...
            ));    }
    public function inscrire_userAction(Request $request){
        $entityManager = $this->getDoctrine()->getManager();
        $username=$request->request->get("_username");
        $password=$request->request->get("_password");

        $user = new User();
        $user->setUsername($username);
        $user->setPassword($password);
        

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($user);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();
        return $this->redirect($this->generateUrl('step2',array('id' => $user->getId())));

    }

    public function step2Action($id)
    {

        return $this->render('TestBundle:Inscrire:step2.html.twig', array('id' => $id
                // ...
            ));    }
    public function inscrire_user_detailsAction(Request $request){
         $entityManager = $this->getDoctrine()->getManager();
        $id_user=$request->request->get("id_user");
        $email=$request->request->get("email");
        $role_user=$request->request->get("role_user");
        $nom=$request->request->get("nom");
        $prenom=$request->request->get("prenom");

        $detailsuser = new Detailsuser();
        $detailsuser->setIdUser((int)$id_user);
        $detailsuser->setEmail($email);
        $detailsuser->setRoleUser($role_user);
        $detailsuser->setNom($nom);
        $detailsuser->setPrenom($prenom);
        

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($detailsuser);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();
        return $this->redirect($this->generateUrl('redirect_user_connect',array('id' => $detailsuser->getId())));
    }
    public function redirect_user_connectAction($id,Request $request){
        $em = $this->getDoctrine()->getManager();
        $detailsuser = $em->getRepository('TestBundle:Detailsuser')->find($id);
        $id_user=$detailsuser->getIdUser();
        $user = $em->getRepository('TestBundle:User')->find($id_user);
        $role_user=$detailsuser->getRoleUser();
        $session = $request->getSession();
$session->start();


// set and get session attributes
$session->set('name', $user->getUsername());
$session->set('loginUserId', $user->getId());
        if($role_user=='client')
        {
             return $this->redirect($this->generateUrl('home_client'));
        }
         if($role_user=='dac')
        {
             return $this->redirect($this->generateUrl('home_dac'));
        }
         if($role_user=='tech_dex')
        {
             return $this->redirect($this->generateUrl('home_techdex'));
        }
         if($role_user=='direct_dex')
        {
             return $this->redirect($this->generateUrl('home_dirdex'));
        }
       
return $this->redirect($this->generateUrl('home_client'));

    }
    public function redirect_loginAction(){

        $usr = $this->get('security.token_storage')->getToken()->getUser();
        $username=$usr->getUsername();
        if($username=="admin"){

            return $this->redirect($this->generateUrl('admin'));
        
        }
       else{
        $id=$usr->getId();
        $repository = $this->getDoctrine()->getRepository(Detailsuser::class);
        $detailsuser = $repository->findOneByIdUser($id);
        $role_user=$detailsuser->getRoleUser();
        
        if($role_user=='client')
        {
             return $this->redirect($this->generateUrl('home_client'));
        }
         if($role_user=='dac')
        {
             return $this->redirect($this->generateUrl('home_dac'));
        }
         if($role_user=='tech_dex')
        {
             return $this->redirect($this->generateUrl('home_techdex'));
        }
         if($role_user=='direct_dex')
        {
             return $this->redirect($this->generateUrl('home_dirdex'));
        }
    }


    }



}

