<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TestBundle\Entity\Transmission;
use TestBundle\Entity\User;


class AdminController extends Controller
{
    public function homeAction()

    {
    	$em = $this->getDoctrine()->getManager();

        $trans = $em->getRepository('TestBundle:Transmission')->findAll();
        $users = $em->getRepository('TestBundle:User')->findAll();
        $equips= $em->getRepository('TestBundle:Equipement')->findAll();
        $commandes= $em->getRepository('TestBundle:Boncommande')->findAll();
        $totaltrans = count((array)$trans);
        $totalusers= count((array)$users);
        $totalequips= count((array)$equips);
        $totalcommandes= count((array)$commandes);
        return $this->render('TestBundle:Admin:home.html.twig', array(
                'totaltrans' => $totaltrans,
                'totalusers' => $totalusers,
                'totalequips' => $totalequips,
                'totalcommandes' => $totalcommandes,
                
            ));    }
    public function consulter_transmissionAction(){
    	$em = $this->getDoctrine()->getManager();

        $trans = $em->getRepository('TestBundle:Transmission')->findAll();
             return $this->render('TestBundle:Admin:transmission.html.twig', array(
                'trans' => $trans,
                
            ));    }
    
    public function consulter_equipementAction(){
    	$em = $this->getDoctrine()->getManager();

        $equips = $em->getRepository('TestBundle:Equipement')->findAll();
    	 return $this->render('TestBundle:Admin:equipement.html.twig', array(
               'equips' => $equips,
                
            ));    }

    
    public function consulter_userAction(){
    	$em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('TestBundle:User')->findAll();
             return $this->render('TestBundle:Admin:user.html.twig', array(
               'users' => $users
                
            ));    }
    
    public function consulter_commandeAction(){
    	$em = $this->getDoctrine()->getManager();

        $commandes = $em->getRepository('TestBundle:Boncommande')->findAll();
              return $this->render('TestBundle:Admin:commande.html.twig', array(
               
                'commandes' => $commandes,
            ));    }
    

}
