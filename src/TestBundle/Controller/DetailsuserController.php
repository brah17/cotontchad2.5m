<?php

namespace TestBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use TestBundle\Entity\Detailsuser;
use TestBundle\Form\DetailsuserType;

/**
 * Detailsuser controller.
 *
 */
class DetailsuserController extends Controller
{

    /**
     * Lists all Detailsuser entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TestBundle:Detailsuser')->findAll();

        return $this->render('TestBundle:Detailsuser:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Detailsuser entity.
     *
     */
    public function createAction(Request $request)
    {

        $entity = new Detailsuser();
        
        $form=$this->createCreateForm($entity);
        
        

      
        $form->handleRequest($request);
        if ($form->isValid()) {
            
          $en=$this->getDoctrine()->getManager();
                $en->persist($entity);
                $en->flush();
            return $this->redirect($this->generateUrl('detailsuser_show', array('id' => $entity->getId())));
        }
    

        return $this->render('TestBundle:Detailsuser:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            
        ));
    }

    /**
     * Creates a form to create a Detailsuser entity.
     *
     * @param Detailsuser $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Detailsuser $entity)
    {
        $form = $this->createForm(new DetailsuserType(), $entity, array(
            'action' => $this->generateUrl('detailsuser_create'),
            'method' => 'POST',
        ));
         $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TestBundle:User')->findAll();
        $ar=array();
        foreach ($entities as $entity) {
            $ar[$entity->getId()]=$entity->getUsername();
        }

      
     $form     ->add('user', 'choice', array(
    'choices' => $ar
,'attr' => array('class' => 'form-control')));
        $form->add('submit', 'submit', array('label' => 'Create','attr' => array('class' => 'btn btn-danger')))
  
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Detailsuser entity.
     *
     */
    public function newAction()
    {
        $entity = new Detailsuser();
        $form   = $this->createCreateForm($entity);
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TestBundle:User')->findAll();
        return $this->render('TestBundle:Detailsuser:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Detailsuser entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TestBundle:Detailsuser')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Detailsuser entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TestBundle:Detailsuser:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Detailsuser entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TestBundle:Detailsuser')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Detailsuser entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TestBundle:Detailsuser:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Detailsuser entity.
    *
    * @param Detailsuser $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Detailsuser $entity)
    {
        $form = $this->createForm(new DetailsuserType(), $entity, array(
            'action' => $this->generateUrl('detailsuser_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update','attr' => array('class' => 'btn btn-danger')));

        return $form;
    }
    /**
     * Edits an existing Detailsuser entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TestBundle:Detailsuser')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Detailsuser entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('detailsuser_edit', array('id' => $id)));
        }

        return $this->render('TestBundle:Detailsuser:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Detailsuser entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TestBundle:Detailsuser')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Detailsuser entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('detailsuser'));
    }

    /**
     * Creates a form to delete a Detailsuser entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('detailsuser_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete','attr' => array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }
}
