<?php

namespace TestBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use TestBundle\Entity\Dac;
use TestBundle\Form\DacType;

/**
 * Dac controller.
 *
 */
class DacController extends Controller
{

    /**
     * Lists all Dac entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TestBundle:Dac')->findAll();

        return $this->render('TestBundle:Dac:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Dac entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Dac();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('dac_show', array('id' => $entity->getId())));
        }

        return $this->render('TestBundle:Dac:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Dac entity.
     *
     * @param Dac $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Dac $entity)
    {
        $form = $this->createForm(new DacType(), $entity, array(
            'action' => $this->generateUrl('dac_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Dac entity.
     *
     */
    public function newAction()
    {
        $entity = new Dac();
        $form   = $this->createCreateForm($entity);

        return $this->render('TestBundle:Dac:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Dac entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TestBundle:Dac')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Dac entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TestBundle:Dac:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Dac entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TestBundle:Dac')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Dac entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TestBundle:Dac:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Dac entity.
    *
    * @param Dac $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Dac $entity)
    {
        $form = $this->createForm(new DacType(), $entity, array(
            'action' => $this->generateUrl('dac_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Dac entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TestBundle:Dac')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Dac entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('dac_edit', array('id' => $id)));
        }

        return $this->render('TestBundle:Dac:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Dac entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TestBundle:Dac')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Dac entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('dac'));
    }

    /**
     * Creates a form to delete a Dac entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('dac_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
