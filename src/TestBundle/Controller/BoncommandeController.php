<?php

namespace TestBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use TestBundle\Entity\Boncommande;
use TestBundle\Form\BoncommandeType;

/**
 * Boncommande controller.
 *
 */
class BoncommandeController extends Controller
{

    /**
     * Lists all Boncommande entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TestBundle:Boncommande')->findAll();

        return $this->render('TestBundle:Boncommande:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Boncommande entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Boncommande();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('boncommande_show', array('id' => $entity->getId())));
        }

        return $this->render('TestBundle:Boncommande:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Boncommande entity.
     *
     * @param Boncommande $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Boncommande $entity)
    {
        $form = $this->createForm(new BoncommandeType(), $entity, array(
            'action' => $this->generateUrl('boncommande_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Boncommande entity.
     *
     */
    public function newAction()
    {
        $entity = new Boncommande();
        $form   = $this->createCreateForm($entity);

        return $this->render('TestBundle:Boncommande:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Boncommande entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TestBundle:Boncommande')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Boncommande entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TestBundle:Boncommande:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Boncommande entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TestBundle:Boncommande')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Boncommande entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TestBundle:Boncommande:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Boncommande entity.
    *
    * @param Boncommande $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Boncommande $entity)
    {
        $form = $this->createForm(new BoncommandeType(), $entity, array(
            'action' => $this->generateUrl('boncommande_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Boncommande entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TestBundle:Boncommande')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Boncommande entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('boncommande_edit', array('id' => $id)));
        }

        return $this->render('TestBundle:Boncommande:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Boncommande entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TestBundle:Boncommande')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Boncommande entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('boncommande'));
    }

    /**
     * Creates a form to delete a Boncommande entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('boncommande_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
